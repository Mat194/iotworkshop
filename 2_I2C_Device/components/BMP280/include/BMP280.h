#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    /** No over-sampling. */
    BMP280_SAMPLING_NONE = 0x00,
    /** 1x over-sampling. */
    BMP280_SAMPLING_X1 = 0x01,
    /** 2x over-sampling. */
    BMP280_SAMPLING_X2 = 0x02,
    /** 4x over-sampling. */
    BMP280_SAMPLING_X4 = 0x03,
    /** 8x over-sampling. */
    BMP280_SAMPLING_X8 = 0x04,
    /** 16x over-sampling. */
    BMP280_SAMPLING_X16 = 0x05
} BMP280_sensor_sampling_e;

/** Operating mode for the sensor. */
typedef enum {
    /** Sleep mode. */
    BMP280_MODE_SLEEP = 0x00,
    /** Forced mode. */
    BMP280_MODE_FORCED = 0x01,
    /** Normal mode. */
    BMP280_MODE_NORMAL = 0x03,
    /** Software reset. */
    BMP280_MODE_SOFT_RESET_CODE = 0xB6
} BMP280_sensor_mode_e;

/** Filtering level for sensor data. */
typedef enum {
    /** No filtering. */
    BMP280_FILTER_OFF = 0x00,
    /** 2x filtering. */
    BMP280_FILTER_X2 = 0x01,
    /** 4x filtering. */
    BMP280_FILTER_X4 = 0x02,
    /** 8x filtering. */
    BMP280_FILTER_X8 = 0x03,
    /** 16x filtering. */
    BMP280_FILTER_X16 = 0x04
} BMP280_sensor_filter_e;

typedef struct {
    uint16_t dig_T1; /**< dig_T1 cal register. */
    int16_t dig_T2;  /**<  dig_T2 cal register. */
    int16_t dig_T3;  /**< dig_T3 cal register. */

    uint16_t dig_P1; /**< dig_P1 cal register. */
    int16_t dig_P2;  /**< dig_P2 cal register. */
    int16_t dig_P3;  /**< dig_P3 cal register. */
    int16_t dig_P4;  /**< dig_P4 cal register. */
    int16_t dig_P5;  /**< dig_P5 cal register. */
    int16_t dig_P6;  /**< dig_P6 cal register. */
    int16_t dig_P7;  /**< dig_P7 cal register. */
    int16_t dig_P8;  /**< dig_P8 cal register. */
    int16_t dig_P9;  /**< dig_P9 cal register. */
} BMP280_calib_data_t;

/** Encapsulates the config register */
typedef struct {
    /** Inactive duration (standby time) in normal mode */
    uint8_t t_sb;
    /** Filter settings */
    uint8_t filter;
    /** Enables 3-wire SPI */
    uint8_t spi3w_en;
} BMP280_config_t;

  /** Encapsulates trhe ctrl_meas register */
typedef struct {
    /** Temperature oversampling. */
    uint8_t osrs_t;
    /** Pressure oversampling. */
    uint8_t osrs_p;
    /** Device mode */
    uint8_t mode;
} BMP280_ctrl_meas_t;

/** Standby duration in ms */
typedef enum {
    /** 1 ms standby. */
    BMP280_STANDBY_MS_1 = 0x00,
    /** 62.5 ms standby. */
    BMP280_STANDBY_MS_63 = 0x01,
    /** 125 ms standby. */
    BMP280_STANDBY_MS_125 = 0x02,
    /** 250 ms standby. */
    BMP280_STANDBY_MS_250 = 0x03,
    /** 500 ms standby. */
    BMP280_STANDBY_MS_500 = 0x04,
    /** 1000 ms standby. */
    BMP280_STANDBY_MS_1000 = 0x05,
    /** 2000 ms standby. */
    BMP280_STANDBY_MS_2000 = 0x06,
    /** 4000 ms standby. */
    BMP280_STANDBY_MS_4000 = 0x07
} BMP280_standby_duration_e;

void BMP280_init(void);
uint8_t BMP280_get_sensor_id(void);
void BMP280_set_sampling(
    BMP280_sensor_mode_e sensor_mode,
    BMP280_sensor_sampling_e temperature_sensor_sampling,
    BMP280_sensor_sampling_e pressure_sensor_sampling,
    BMP280_sensor_filter_e sensor_filter,
    BMP280_standby_duration_e standby_duration
);
void BMP280_get_calibration_data(BMP280_calib_data_t* calib_data);
float BMP280_read_temperature(void);
float BMP280_read_pressure(void);

#ifdef __cplusplus
}
#endif