#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BMP280_ADDRESS       (0x77)   /**< The default I2C address for the sensor. */
#define BMP280_ADDRESS_ALT   (0x76)   /**< Alternative I2C address for the sensor. */
#define BMP280_CHIPID        (0x58)   /**< Default chip ID. */

typedef enum {
    BMP280_REGISTER_DIG_T1 = 0x88,
    BMP280_REGISTER_DIG_T2 = 0x8A,
    BMP280_REGISTER_DIG_T3 = 0x8C,
    BMP280_REGISTER_DIG_P1 = 0x8E,
    BMP280_REGISTER_DIG_P2 = 0x90,
    BMP280_REGISTER_DIG_P3 = 0x92,
    BMP280_REGISTER_DIG_P4 = 0x94,
    BMP280_REGISTER_DIG_P5 = 0x96,
    BMP280_REGISTER_DIG_P6 = 0x98,
    BMP280_REGISTER_DIG_P7 = 0x9A,
    BMP280_REGISTER_DIG_P8 = 0x9C,
    BMP280_REGISTER_DIG_P9 = 0x9E,
    BMP280_REGISTER_CHIPID = 0xD0,
    BMP280_REGISTER_VERSION = 0xD1,
    BMP280_REGISTER_SOFTRESET = 0xE0,
    BMP280_REGISTER_CAL26 = 0xE1, /**< R calibration = 0xE1-0xF0 */
    BMP280_REGISTER_STATUS = 0xF3,
    BMP280_REGISTER_CONTROL = 0xF4,
    BMP280_REGISTER_CONFIG = 0xF5,
    BMP280_REGISTER_PRESSUREDATA = 0xF7,
    BMP280_REGISTER_TEMPDATA = 0xFA,
} BMP280_register_e;

uint8_t BMP280_i2c_read_8(uint8_t reg);
uint16_t BMP280_i2c_read_16(uint8_t reg);
uint16_t BMP280_i2c_read_16_le(uint8_t reg);
int16_t BMP280_i2c_read_s16_le(uint8_t reg);
uint32_t BMP280_i2c_read_24(uint8_t reg);
void BMP280_i2c_write_8(uint8_t reg, uint8_t value);
void BMP280_i2c_init(void);

#ifdef __cplusplus
}
#endif