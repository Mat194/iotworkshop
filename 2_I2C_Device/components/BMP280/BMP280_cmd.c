#include "BMP280_cmd.h"
#include "esp_console.h"
#include "esp_log.h"
#include "BMP280.h"

static const char *TAG = "bmp280";

static int do_bmp280_read_sensor_id_cmd(int argc, char **argv) {
    uint8_t sensor_id = BMP280_get_sensor_id();
    ESP_LOGI(TAG, "The sensor ID is 0x%02X", sensor_id);
    return 0;
}

static void register_bmp280_read_sensor_id(void) {
    const esp_console_cmd_t bmp280_read_sensor_id_cmd = {
        .command = "bmp280_read_sensor_id",
        .help = "Read sensor ID BM280",
        .hint = NULL,
        .func = &do_bmp280_read_sensor_id_cmd,
        .argtable = NULL
    };
    ESP_ERROR_CHECK(esp_console_cmd_register(&bmp280_read_sensor_id_cmd));
}

static int do_bmp280_print_calibration_data_cmd(int argc, char **argv) {
    BMP280_calib_data_t calib_data;
    BMP280_get_calibration_data(&calib_data);

    printf("Calib data: \r\n");
    printf("DIG_T1: %u\r\n", calib_data.dig_T1);
    printf("DIG_T2: %d\r\n", calib_data.dig_T2);
    printf("DIG_T3: %d\r\n", calib_data.dig_T3);
    printf("DIG_P1: %u\r\n", calib_data.dig_P1);
    printf("DIG_P2: %d\r\n", calib_data.dig_P2);
    printf("DIG_P3: %d\r\n", calib_data.dig_P3);
    printf("DIG_P4: %d\r\n", calib_data.dig_P4);
    printf("DIG_P5: %d\r\n", calib_data.dig_P5);
    printf("DIG_P6: %d\r\n", calib_data.dig_P6);
    printf("DIG_P7: %d\r\n", calib_data.dig_P7);
    printf("DIG_P8: %d\r\n", calib_data.dig_P8);
    printf("DIG_P9: %d\r\n", calib_data.dig_P9);
    return 0;
}

static void register_bmp280_print_calibration_data_cmd(void) {
    const esp_console_cmd_t bmp280_print_calibration_data = {
        .command = "bmp280_print_calibration_data",
        .help = "Print calibration data of BM280",
        .hint = NULL,
        .func = &do_bmp280_print_calibration_data_cmd,
        .argtable = NULL
    };
    ESP_ERROR_CHECK(esp_console_cmd_register(&bmp280_print_calibration_data));
}

static int do_bmp280_read_temperature_cmd(int argc, char **argv) {
    float temperature = BMP280_read_temperature();
    printf("Temperature: %f\n\r", temperature);
    return 0;
}

static void register_bmp280_read_temperature_cmd(void) {
    const esp_console_cmd_t bmp280_read_temperature = {
        .command = "bmp280_read_temperature",
        .help = "Read temperature from BM280",
        .hint = NULL,
        .func = &do_bmp280_read_temperature_cmd,
        .argtable = NULL
    };
    ESP_ERROR_CHECK(esp_console_cmd_register(&bmp280_read_temperature));
}

static int do_bmp280_read_pressure_cmd(int argc, char **argv) {
    float pressure = BMP280_read_pressure();
    printf("Pressure: %f\n\r", pressure);
    return 0;
}

static void register_bmp280_read_pressure_cmd(void) {
    const esp_console_cmd_t bmp280_read_pressure = {
        .command = "bmp280_read_pressure",
        .help = "Read pressure from BM280",
        .hint = NULL,
        .func = &do_bmp280_read_pressure_cmd,
        .argtable = NULL
    };
    ESP_ERROR_CHECK(esp_console_cmd_register(&bmp280_read_pressure));
}

void register_bmp280_cmd(void) {
    BMP280_init();
    register_bmp280_read_sensor_id();
    register_bmp280_print_calibration_data_cmd();
    register_bmp280_read_temperature_cmd();
    register_bmp280_read_pressure_cmd();
}