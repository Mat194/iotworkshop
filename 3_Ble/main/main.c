#include <stdio.h>
#include "sdkconfig.h"
#include "esp_log.h"
#include "esp_console.h"
#include "cmd_system.h"
#include "BMP280_cmd.h"
#include "BMP280.h"
#include "BLE.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

void temperature_update_ble_task(void *pvParameters)
{
    ESP_LOGI("Main", "Temp BLE task started\n");
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    for(;;) {
        BLE_update_temperature_value(BMP280_read_temperature());
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

void app_main(void)
{
    esp_err_t ret;

    // Initialize NVS.
    ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    esp_console_repl_t *repl = NULL;
    esp_console_repl_config_t repl_config = ESP_CONSOLE_REPL_CONFIG_DEFAULT();

    repl_config.prompt = "iot_device>";

    // install console REPL environment
    esp_console_dev_uart_config_t uart_config = ESP_CONSOLE_DEV_UART_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_console_new_repl_uart(&uart_config, &repl_config, &repl));

    register_system();
    register_bmp280_cmd();

    BLE_start();

    xTaskCreate(temperature_update_ble_task, "temp_ble_task", 3048, 0, 8, NULL);

    ESP_ERROR_CHECK(esp_console_start_repl(repl));
}
