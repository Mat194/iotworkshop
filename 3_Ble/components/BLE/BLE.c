#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"


#include "freertos/semphr.h"
#include "BLE.h"

#define LOG_TAG "BLE"
#define BLE_APP_ID              0x56
#define SAMPLE_DEVICE_NAME          "IOT Device"    //The Device Name Characteristics in GAP

struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
    uint16_t char_handle;
    esp_bt_uuid_t char_uuid;
    esp_gatt_perm_t perm;
    esp_gatt_char_prop_t property;
    uint16_t descr_handle;
    esp_bt_uuid_t descr_uuid;
};

static const uint8_t adv_data[23] = {
    /* Flags */
    0x02,0x01,0x06,
    /* Complete List of 16-bit Service Class UUIDs */
    0x03,0x03,0xF0,0xAB,
    /* Complete Local Name in advertising */
    0x08,0x09, 'E', 'S', 'P', '_', 'I', 'O', 'T',
    0x05,0xFF, 0xFF, 0xFF, 0x03, 0x04
};

static uint8_t adv_config_done = 0;
#define adv_config_flag      (1 << 0)
#define scan_rsp_config_flag (1 << 1)

#define ATTR_DB_SIZE 4

static uint16_t mtu_size = 23;
static uint16_t handle_table[ATTR_DB_SIZE];

static uint8_t find_char_and_desr_index(uint16_t handle)
{
    uint8_t error = 0xff;

    for(int i = 0; i < ATTR_DB_SIZE ; i++){
        if(handle == handle_table[i]){
            return i;
        }
    }

    return error;
}

static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x20,
    .adv_int_max        = 0x40,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

static bool is_connected = false;

static void gatts_app_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);


static const uint16_t primary_service_uuid = ESP_GATT_UUID_PRI_SERVICE;
static const uint16_t character_declaration_uuid = ESP_GATT_UUID_CHAR_DECLARE;
static const uint16_t character_client_config_uuid = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;

static const uint16_t app_service_uuid = 0xABF0;
//static const uint8_t char_prop_read_write = ESP_GATT_CHAR_PROP_BIT_WRITE_NR|ESP_GATT_CHAR_PROP_BIT_READ;
static const uint8_t char_prop_read_notify = ESP_GATT_CHAR_PROP_BIT_READ|ESP_GATT_CHAR_PROP_BIT_NOTIFY;

static const uint16_t app_temperature_data_read_uuid = 0xABF1;
static const uint8_t  app_temperature_data_value[2] = {0x00};

static const uint8_t  app_temperature_data_notify_ccc[2] = {0x00, 0x00};


static esp_gatt_if_t app_gatts_if = 0xff;
static uint16_t app_conn_id = 0xffff;


typedef enum {
    APP_SERVICE_DECL = 0,
    APP_TEMPERATURE_DATA_CHARACTERISTIC_DECLARATION,
    APP_TEMPERATURE_DATA_CHARACTERISTIC_VALUE,
    APP_TEMPERATURE_DATA_CHARACTERISTIC_CCC
};

static const esp_gatts_attr_db_t gatt_db[] =
{
    // Service Declaration
    [APP_SERVICE_DECL]                      	=
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&primary_service_uuid, ESP_GATT_PERM_READ,
    sizeof(app_service_uuid), sizeof(app_service_uuid), (uint8_t *)&app_service_uuid}},

    // APP Temperature Data Read Characteristic Declaration
    [APP_TEMPERATURE_DATA_CHARACTERISTIC_DECLARATION] =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
    1,1, (uint8_t *)&char_prop_read_notify}},

    // APP Temperature Data Read Characteristic Value
    [APP_TEMPERATURE_DATA_CHARACTERISTIC_VALUE] =
    {{ESP_GATT_RSP_BY_APP}, {ESP_UUID_LEN_16, (uint8_t *)&app_temperature_data_read_uuid, ESP_GATT_PERM_READ,
    2, sizeof(app_temperature_data_value), (uint8_t *)app_temperature_data_value}},

    // APP Temperature Data Read - Client Characteristic Configuration Descriptor
    [APP_TEMPERATURE_DATA_CHARACTERISTIC_CCC]         =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_client_config_uuid, ESP_GATT_PERM_READ|ESP_GATT_PERM_WRITE,
    sizeof(uint16_t),sizeof(app_temperature_data_notify_ccc), (uint8_t *)app_temperature_data_notify_ccc}},
};


/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst profile_tab[1] = {
    [0] = {
        .gatts_cb = gatts_app_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
};

static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    esp_err_t err;

    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&adv_params);
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if((err = param->adv_start_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(LOG_TAG, "Advertising start failed: %s\n", esp_err_to_name(err));
        }
        break;
    default:
        break;
    }
}

static void gatts_app_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    esp_ble_gatts_cb_param_t *p_data = (esp_ble_gatts_cb_param_t *) param;
    uint8_t res = 0xff;
    switch (event) {
    	case ESP_GATTS_REG_EVT:
    	    ESP_LOGI(LOG_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gap_set_device_name(SAMPLE_DEVICE_NAME);

        	ESP_LOGI(LOG_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gap_config_adv_data_raw((uint8_t *)adv_data, sizeof(adv_data));

        	ESP_LOGI(LOG_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gatts_create_attr_tab(gatt_db, gatts_if, ATTR_DB_SIZE, 0);
       	break;
    	case ESP_GATTS_READ_EVT:
            ESP_LOGI(LOG_TAG, "GATT_READ_EVT, conn_id %d, trans_id %d, handle %d\n", param->read.conn_id, param->read.trans_id, param->read.handle);

            res = find_char_and_desr_index(p_data->read.handle);
            ESP_LOGI(LOG_TAG, "Found handle %d for %d", res, p_data->read.handle);
            
            if (res == APP_TEMPERATURE_DATA_CHARACTERISTIC_VALUE) {
                esp_gatt_rsp_t rsp;
                memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
                rsp.attr_value.handle = param->read.handle;
                rsp.attr_value.len = 2;
                rsp.attr_value.value[0] = 0xde;
                rsp.attr_value.value[2] = 0xad;
                esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id, ESP_GATT_OK, &rsp);
            }
       	 break;
    	case ESP_GATTS_WRITE_EVT: {
    	    res = find_char_and_desr_index(p_data->write.handle);
            
      	 	break;
    	}
    	case ESP_GATTS_EXEC_WRITE_EVT:{
    	    break;
    	}
    	case ESP_GATTS_MTU_EVT:
    	    mtu_size = p_data->mtu.mtu;
    	    break;
    	case ESP_GATTS_CONF_EVT:
    	    break;
    	case ESP_GATTS_UNREG_EVT:
        	break;
    	case ESP_GATTS_DELETE_EVT:
        	break;
    	case ESP_GATTS_START_EVT:
        	break;
    	case ESP_GATTS_STOP_EVT:
        	break;
    	case ESP_GATTS_CONNECT_EVT:
    	    app_conn_id = p_data->connect.conn_id;
    	    app_gatts_if = gatts_if;
    	    is_connected = true;
        	break;
    	case ESP_GATTS_DISCONNECT_EVT:
    	    is_connected = false;
    	    esp_ble_gap_start_advertising(&adv_params);
    	    break;
    	case ESP_GATTS_OPEN_EVT:
    	    break;
    	case ESP_GATTS_CANCEL_OPEN_EVT:
    	    break;
    	case ESP_GATTS_CLOSE_EVT:
    	    break;
    	case ESP_GATTS_LISTEN_EVT:
    	    break;
    	case ESP_GATTS_CONGEST_EVT:
    	    break;
    	case ESP_GATTS_CREAT_ATTR_TAB_EVT:{
    	    ESP_LOGI(LOG_TAG, "The number handle =%x\n",param->add_attr_tab.num_handle);
    	    if (param->add_attr_tab.status != ESP_GATT_OK){
    	        ESP_LOGE(LOG_TAG, "Create attribute table failed, error code=0x%x", param->add_attr_tab.status);
    	    }
    	    else if (param->add_attr_tab.num_handle != ATTR_DB_SIZE){
    	        ESP_LOGE(LOG_TAG, "Create attribute table abnormally, num_handle (%d) doesn't equal to HRS_IDX_NB(%d)", param->add_attr_tab.num_handle, ATTR_DB_SIZE);
    	    }
    	    else {
    	        memcpy(handle_table, param->add_attr_tab.handles, sizeof(handle_table));
    	        esp_ble_gatts_start_service(handle_table[0]);
    	    }
    	    break;
    	}
    	default:
    	    break;
    }
}

static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            profile_tab[0].gatts_if = gatts_if;
        } else {
            ESP_LOGI(LOG_TAG, "Reg app failed, app_id %04x, status %d\n",param->reg.app_id, param->reg.status);
            return;
        }
    }

    do {
        if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
            gatts_if == profile_tab[0].gatts_if) {
            if (profile_tab[0].gatts_cb) {
                profile_tab[0].gatts_cb(event, gatts_if, param);
            }
        }
    } while (0);
}


void BLE_start(void)
{
    esp_err_t ret;
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    ret = esp_bluedroid_init();
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s init bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    ret = esp_bluedroid_enable();
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s enable bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    ret = esp_ble_gatts_register_callback(gatts_event_handler);
    if (ret){
        ESP_LOGE(LOG_TAG, "gatts register error, error code = %x", ret);
        return;
    }

    ret = esp_ble_gap_register_callback(gap_event_handler);
    if (ret){
        ESP_LOGE(LOG_TAG, "gap register error, error code = %x", ret);
        return;
    }

    ret = esp_ble_gatts_app_register(0);
    if (ret){
        ESP_LOGE(LOG_TAG, "gatts app register error, error code = %x", ret);
        return;
    }

    esp_err_t local_mtu_ret = esp_ble_gatt_set_local_mtu(500);
    if (local_mtu_ret){
        ESP_LOGE(LOG_TAG, "set local  MTU failed, error code = %x", local_mtu_ret);
    }
}

void BLE_update_temperature_value(float temperature) {
    if (!is_connected) {
        return;
    }

    uint16_t temp = (uint16_t)(temperature * 10.0f);
    esp_ble_gatts_send_indicate(app_gatts_if, app_conn_id, handle_table[APP_TEMPERATURE_DATA_CHARACTERISTIC_VALUE], 2, &temp, false);
}
