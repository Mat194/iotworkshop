#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void BLE_start(void);
void BLE_update_temperature_value(float temperature);

#ifdef __cplusplus
}
#endif
