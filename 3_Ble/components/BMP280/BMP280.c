#include <stdio.h>
#include <string.h>
#include "BMP280.h"
#include "BMP280_i2c.h"
#include "esp_log.h"

static BMP280_calib_data_t _calib_data;
static BMP280_config_t _config;
static BMP280_ctrl_meas_t _ctrl_meas;
static int32_t t_fine;

static void read_calib_data(void);

void BMP280_init(void) {
    _config.t_sb = BMP280_STANDBY_MS_1;
    _config.filter = BMP280_FILTER_OFF;
    _config.spi3w_en = 0;

    _ctrl_meas.osrs_t = BMP280_SAMPLING_NONE;
    _ctrl_meas.osrs_p = BMP280_SAMPLING_NONE;
    _ctrl_meas.mode = BMP280_MODE_SLEEP;

    BMP280_i2c_init();

    BMP280_set_sampling(
        BMP280_MODE_NORMAL,
        BMP280_SAMPLING_X16,
        BMP280_SAMPLING_X16,
        BMP280_FILTER_OFF,
        BMP280_STANDBY_MS_1
    );

    read_calib_data();
}

uint8_t BMP280_get_sensor_id(void) {
    return BMP280_i2c_read_8(BMP280_REGISTER_CHIPID);
}

void BMP280_set_sampling(
    BMP280_sensor_mode_e sensor_mode,
    BMP280_sensor_sampling_e temperature_sensor_sampling,
    BMP280_sensor_sampling_e pressure_sensor_sampling,
    BMP280_sensor_filter_e sensor_filter,
    BMP280_standby_duration_e standby_duration
) {
    _ctrl_meas.mode = sensor_mode;
    _ctrl_meas.osrs_t = temperature_sensor_sampling;
    _ctrl_meas.osrs_p = pressure_sensor_sampling;

    _config.filter = sensor_filter;
    _config.t_sb = standby_duration;

    uint8_t config_byte = 
        (_config.t_sb << 5) | 
        (_config.filter << 2) |
        _config.spi3w_en;

    uint8_t ctrl_meas_byte = 
        (_ctrl_meas.osrs_t << 5) |
        (_ctrl_meas.osrs_p << 2) |
        _ctrl_meas.mode;
    BMP280_i2c_write_8(BMP280_REGISTER_CONFIG, config_byte);
    BMP280_i2c_write_8(BMP280_REGISTER_CONTROL, ctrl_meas_byte);
}

void BMP280_get_calibration_data(BMP280_calib_data_t* calib_data) {
    memcpy(calib_data, &_calib_data, sizeof(BMP280_calib_data_t));
}

float BMP280_read_temperature(void) {
    int32_t var1, var2;

    int32_t adc_T = BMP280_i2c_read_24(BMP280_REGISTER_TEMPDATA);
    adc_T >>= 4;

    var1 = ((((adc_T >> 3) - ((int32_t)_calib_data.dig_T1 << 1))) *
        ((int32_t)_calib_data.dig_T2)) >> 11;

    var2 = (((((adc_T >> 4) - ((int32_t)_calib_data.dig_T1)) *
        ((adc_T >> 4) - ((int32_t)_calib_data.dig_T1))) >> 12) *
        ((int32_t)_calib_data.dig_T3)) >> 14;

    t_fine = var1 + var2;

    float T = (t_fine * 5 + 128) >> 8;
    return T / 100;
}

float BMP280_read_pressure(void) {
    int64_t var1, var2, p;

    // Must be done first to get the t_fine variable set up
    BMP280_read_temperature();

    int32_t adc_P = BMP280_i2c_read_24(BMP280_REGISTER_PRESSUREDATA);
    adc_P >>= 4;

    var1 = ((int64_t)t_fine) - 128000;
    var2 = var1 * var1 * (int64_t)_calib_data.dig_P6;
    var2 = var2 + ((var1 * (int64_t)_calib_data.dig_P5) << 17);
    var2 = var2 + (((int64_t)_calib_data.dig_P4) << 35);
    var1 = ((var1 * var1 * (int64_t)_calib_data.dig_P3) >> 8) +
           ((var1 * (int64_t)_calib_data.dig_P2) << 12);
    var1 =
        (((((int64_t)1) << 47) + var1)) * ((int64_t)_calib_data.dig_P1) >> 33;

    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
  
    p = 1048576 - adc_P;
    p = (((p << 31) - var2) * 3125) / var1;
    var1 = (((int64_t)_calib_data.dig_P9) * (p >> 13) * (p >> 13)) >> 25;
    var2 = (((int64_t)_calib_data.dig_P8) * p) >> 19;

    p = ((p + var1 + var2) >> 8) + (((int64_t)_calib_data.dig_P7) << 4);
    return (float)p / 256;
}

static void read_calib_data(void) {
    _calib_data.dig_T1 = BMP280_i2c_read_16_le(BMP280_REGISTER_DIG_T1);
    _calib_data.dig_T2 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_T2);
    _calib_data.dig_T3 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_T3);

    _calib_data.dig_P1 = BMP280_i2c_read_16_le(BMP280_REGISTER_DIG_P1);
    _calib_data.dig_P2 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P2);
    _calib_data.dig_P3 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P3);
    _calib_data.dig_P4 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P4);
    _calib_data.dig_P5 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P5);
    _calib_data.dig_P6 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P6);
    _calib_data.dig_P7 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P7);
    _calib_data.dig_P8 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P8);
    _calib_data.dig_P9 = BMP280_i2c_read_s16_le(BMP280_REGISTER_DIG_P9);
}
