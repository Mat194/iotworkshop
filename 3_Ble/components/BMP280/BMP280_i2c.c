#include "BMP280_i2c.h"
#include "driver/i2c.h"

static gpio_num_t i2c_gpio_sda = 41;
static gpio_num_t i2c_gpio_scl = 42;
static uint32_t i2c_frequency = 100000;
static i2c_port_t i2c_port = I2C_NUM_0;

static esp_err_t i2c_master_driver_initialize(void)
{
    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = i2c_gpio_sda,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_io_num = i2c_gpio_scl,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = i2c_frequency,
    };
    return i2c_param_config(i2c_port, &conf);
}

uint8_t BMP280_i2c_read_8(uint8_t reg) {
    uint8_t buf = { reg };
    uint8_t response = 0;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    
    esp_err_t err = i2c_master_write_read_device(
        i2c_port, 
        BMP280_ADDRESS_ALT,
        &buf, 
        1,
        &response, 
        1,
        1000 / portTICK_RATE_MS);

    i2c_master_stop(cmd);
    i2c_cmd_link_delete(cmd);
    return response;
}

uint16_t BMP280_i2c_read_16(uint8_t reg) {
    uint8_t buffer[2];
    buffer[0] = reg;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    
    esp_err_t err = i2c_master_write_read_device(
        i2c_port, 
        BMP280_ADDRESS_ALT,
        buffer, 
        2,
        buffer, 
        2,
        1000 / portTICK_RATE_MS);

    i2c_master_stop(cmd);
    i2c_cmd_link_delete(cmd);
    return ((uint16_t)buffer[0]) << 8 | ((uint16_t)buffer[1]);
}

uint16_t BMP280_i2c_read_16_le(uint8_t reg) {
    uint16_t temp = BMP280_i2c_read_16(reg);
    return (temp >> 8) | (temp << 8);
}

int16_t BMP280_i2c_read_s16_le(uint8_t reg) {
    return (int16_t)BMP280_i2c_read_16_le(reg);
}

uint32_t BMP280_i2c_read_24(uint8_t reg) {
    uint8_t buffer[3];
    buffer[0] = reg;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    
    esp_err_t err = i2c_master_write_read_device(
        i2c_port, 
        BMP280_ADDRESS_ALT,
        buffer, 
        2,
        buffer, 
        2,
        1000 / portTICK_RATE_MS);

    i2c_master_stop(cmd);
    i2c_cmd_link_delete(cmd);
    return ((uint32_t)buffer[0]) << 16 | ((uint32_t)buffer[1]) << 8 | ((uint32_t)buffer[2]);
}

void BMP280_i2c_write_8(uint8_t reg, uint8_t value) {
    uint8_t buffer[2] = { reg, value };

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);

    esp_err_t err = i2c_master_write_to_device(
        i2c_port, 
        BMP280_ADDRESS_ALT,
        buffer, 
        2,
        1000 / portTICK_RATE_MS);

    i2c_master_stop(cmd);
    i2c_cmd_link_delete(cmd);
}

void BMP280_i2c_init(void) {
    i2c_driver_install(i2c_port, I2C_MODE_MASTER, 0, 0, 0);
    i2c_master_driver_initialize();
}